import DisplayCategory from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/DisplayCategory';

export interface Part {
  category: DisplayCategory;
  mappers: { [index: string]: Mapper };
  name: string;
  services: string[];
  site: number | string;
  traits: { [index: string]: number };
}

export interface Parts {
  [index: string]: Part;
}

export interface Mapper {
  [index: string]: any;
}

export default interface Model {
  identity: string;
  parts: Parts;
}
