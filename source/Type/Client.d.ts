export default interface Client {
  clientId: string;
  name: string;
  redirectUri: string;
  secret: string;
  xUser: { identity: string; [index: string]: any; };
}
